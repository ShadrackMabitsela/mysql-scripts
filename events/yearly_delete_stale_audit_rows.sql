-- Create an event that executes yearly for the next 10 years.
-- It will delete the row(s) older than two years.
USE sql_invoicing;

-- Delete the event.
DROP EVENT IF EXISTS yearly_delete_stale_audit_rows;

-- Set the delimiter.
DELIMITER $$

CREATE EVENT yearly_delete_stale_audit_rows
ON SCHEDULE EVERY 1 YEAR STARTS '2022-01-01' ENDS '2032-01-02'
DO BEGIN
    -- delete the records older that 2 years.
    DELETE FROM payments_audit
    WHERE action_date < (NOW() - INTERVAL 2 YEAR);
END $$

-- Reset the delimiter.
DELIMITER ;
