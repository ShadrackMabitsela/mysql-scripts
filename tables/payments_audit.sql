-- Creates a table that will be used
-- by the triggers for auditing purposes.
USE sql_invoicing;

CREATE TABLE payments_audit
(
    client_id INT,
    date DATE,
    amount DECIMAL(9,2),
    action_type VARCHAR(50),
    action_date DATETIME
);
