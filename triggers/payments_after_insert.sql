-- Executed when a payment is inserted.
USE sql_invoicing;

-- Delete the trigger.
DROP TRIGGER IF EXISTS payments_after_insert;

-- Set the delimiter.
DELIMITER $$

CREATE TRIGGER payments_after_insert
	AFTER DELETE ON payments
	FOR EACH ROW
BEGIN
	UPDATE invoices i
	SET payment_total = payment_total + NEW.amount
	WHERE i.invoice_id = NEW.invoice_id;
    
    -- Populate the payments_audit table.
    INSERT INTO payments_audit
    VALUES(
        NEW.client_id,
        NEW.date,
        NEW.amount,
        'INSERT',
        NOW()
	);
END $$

-- Reset the delimiter.
DELIMITER ;

-- Execute the trigger.
-- INSERT INTO payments
-- (payment_id, client_id, invoice_id, date, amount, payment_method)
-- VALUES(DEFAULT, 5, 3, '2022-02-19', 115, 1);
