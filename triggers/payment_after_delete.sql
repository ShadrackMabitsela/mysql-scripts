-- Executed when a payment is deleted.
USE sql_invoicing;

-- Delete the trigger.
DROP TRIGGER IF EXISTS payment_after_delete;

-- Set the delimiter.
DELIMITER $$

CREATE TRIGGER payment_after_delete
	AFTER DELETE ON payments
	FOR EACH ROW
BEGIN
	UPDATE invoices i
	SET payment_total = payment_total - OLD.amount
	WHERE i.invoice_id = OLD.invoice_id;
    
    -- Populate the payments_audit table.
    INSERT INTO payments_audit
    VALUES(
        OLD.client_id,
        OLD.date,
        OLD.amount,
        'DELETE',
        NOW()
	);
END $$

-- Reset the delimiter.
DELIMITER ;

-- Execute the trigger.
-- DELETE FROM payments
-- WHERE payment_id = 15;
