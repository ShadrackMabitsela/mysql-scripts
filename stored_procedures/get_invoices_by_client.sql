-- Returns invoices for a given client id.
USE sql_invoicing;

-- Delete the stored procedure.
DROP PROCEDURE IF EXISTS get_invoices_by_client;

-- Set the default delimiter to '$$'.
DELIMITER $$
CREATE PROCEDURE get_invoices_by_client(client_id INT)
	BEGIN
		IF (client_id IS NULL) OR (client_id <= 0) THEN
			SELECT 'NULL, 0 or less entered.' AS 'Bad Input';
		ELSE
			SELECT *
			FROM invoices i
			WHERE i.client_id = client_id;
		END IF;
	END $$

-- Reset the default delimiter back to the semicolon.
DELIMITER ;

-- Call the procedure.
CALL get_invoices_by_client(1);
