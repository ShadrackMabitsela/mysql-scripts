-- Gets payments by client_id, payment_method_id, or both.
USE sql_invoicing;

-- Delete the procedure.
DROP PROCEDURE IF EXISTS get_payments;

-- Set the delimiter.
DELIMITER $$

CREATE PROCEDURE get_payments(
	client_id INT,
	payment_method_id TINYINT
)
	BEGIN
		SELECT *
		FROM clients c
		INNER JOIN payments p USING (client_id)
		INNER JOIN payment_methods pm
			ON p.payment_method = pm.payment_method_id
		WHERE c.client_id = IFNULL(client_id, c.client_id)
			AND pm.payment_method_id = IFNULL(payment_method_id, pm.payment_method_id);
	END $$

-- Reset the default delimiter back to ';'.
DELIMITER ;

-- Call the stored procedure.
CALL get_payments(NULL, NULL);
-- CALL get_payments(3, NULL);
-- CALL get_payments(5, 2);
-- CALL get_payments(NULL, 2);
