-- A stored procedure that gets invoices with a balance greater that 0.

USE sql_invoicing;

-- Delete the procedure.
DROP PROCEDURE IF EXISTS get_invoices_with_balance;

-- Set the delimiter to '$$'.
-- In order to group all the statements in the stored procedure as one unit. 
DELIMITER $$ 

CREATE PROCEDURE get_invoices_with_balance()
	BEGIN
		SELECT *, (invoice_total - payment_total) AS balance
		FROM invoices
		WHERE (invoice_total - payment_total) > 0;
	END $$

-- Change the default delimiter back to the semicolon.
DELIMITER ;


-- Call the procedure.
CALL get_invoices_with_balance();
