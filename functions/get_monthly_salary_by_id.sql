-- Gets the monthly salary/wage paid to an
-- based on the employee_id given.
USE sql_hr;

-- Delete the function.
DROP FUNCTION IF EXISTS get_monthly_salary_by_id;

-- Set the delimiter.
DELIMITER $$

CREATE FUNCTION get_monthly_salary_by_id(id INT)
RETURNS DECIMAL(10, 2)
READS SQL DATA
BEGIN
	DECLARE monthly_salary DECIMAL(10, 2) DEFAULT 0;
    
	SELECT salary / 12
    INTO monthly_salary
    FROM employees e
    WHERE e.employee_id = id;
    
    RETURN monthly_salary;
END $$

-- Reset the delimiter.
DELIMITER ;

-- Execute the function.
SELECT get_monthly_salary_by_id(76196) AS monthly_salary;
-- SELECT get_monthly_salary_by_id(84791) AS monthly_salary;
-- SELECT get_monthly_salary_by_id(37851) AS monthly_salary;
-- SELECT get_monthly_salary_by_id(68249) AS monthly_salary;
