-- Gets the sum total of all the yearly salaries
-- paid to the company's employees annually.
USE sql_hr;

-- Delete the function.
DROP FUNCTION IF EXISTS get_total_salaries_paid;

DELIMITER $$

CREATE FUNCTION get_total_salaries_paid()
-- Return statement that specifies
-- the return values data type.
RETURNS DECIMAL(9,2)
-- Set the attribute(s) of the function.
-- Each MySql function must have at least one attributes.
READS SQL DATA

BEGIN
	DECLARE total_salaries DECIMAL(10, 2) DEFAULT 0;

	SELECT SUM(salary)
	INTO total_salaries
	FROM employees;

	RETURN total_salaries;
END; $$

-- Reset the delimiter.
DELIMITER ;

-- Execute the function.
SELECT get_total_salaries_paid() AS 'Total_Salaries_Paid';
