-- This view determines the membership standard of a customer,
-- based on the points they have.
USE sql_store;

CREATE OR REPLACE VIEW membership_standard AS
	SELECT
		*,
		CASE
			WHEN points > 3000 THEN 'GOLD'
			WHEN points BETWEEN 2000 AND 3000 THEN 'SILVER'
			WHEN points BETWEEN 0 AND 1999 THEN 'BRONZE'
			ELSE
				'INVALID VALUE'
		END AS 'membership_standard'
	FROM customers
	ORDER BY points DESC;

-- Execute the view.
SELECT *
FROM membership_standard;

-- Delete the view.
-- DROP VIEW IF EXISTS membership_standard;
