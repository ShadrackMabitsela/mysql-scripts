-- This script creates a view that gets balance owed by each client.

USE sql_invoicing;

CREATE OR REPLACE VIEW clients_balance AS
	SELECT
		c.client_id,
		c.name,
		(SUM(i.invoice_total) - SUM(i.payment_total)) AS balance
	FROM clients c
	INNER JOIN invoices i USING (client_id)
	GROUP BY c.client_id, c.name
	ORDER BY balance DESC;


-- Execute the view.
SELECT * 
FROM clients_balance;

-- Delete the view
-- DROP VIEW IF EXISTS clients_balance;
